package com.pik.moviecollection.server.model;

/**
 * Created by spiczek on 08.05.14.
 */
public class Security {
    private static Security ourInstance = new Security();

    public static Security getInstance() {
        return ourInstance;
    }

    private Security() {
    }

    public static int validateToken(String token) {

        if (token.equals("super_user"))
            return 0;
        else
            return 1;

    }
}
