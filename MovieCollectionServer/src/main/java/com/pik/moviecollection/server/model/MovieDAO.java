package com.pik.moviecollection.server.model;

import com.pik.moviecollection.server.model.Movie;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by spiczek on 08.05.14.
 */
public class MovieDAO {

    private static MovieDAO ourInstance = new MovieDAO();
    public static MovieDAO getInstance() {
        return ourInstance;
    }

    private MovieDAO() {
    }

    private static List<Movie> items = new LinkedList<Movie>();

    static {
        items.add(new Movie(0));
        items.add(new Movie(1));
        items.add(new Movie(2));
        items.add(new Movie(3));
        items.add(new Movie(4));
    }


    public List<Movie> getAvailableMovies() {
        return items;
    }
}
