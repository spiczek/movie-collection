package com.pik.moviecollection.server.api;

import com.pik.moviecollection.server.model.Movie;
import com.pik.moviecollection.server.model.MovieDAO;
import com.pik.moviecollection.server.model.Security;
import com.sun.tracing.dtrace.ProviderAttributes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by spiczek on 08.05.14.
 */

@RestController
@RequestMapping("/api/movie")
@ResponseStatus(HttpStatus.OK)
public class MovieService {

    /*
    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public String getGreeting(@PathVariable String name) {
        String result="Hello "+name;
        return result;
        //return new ResponseEntity<String>("{value: " + result + "}", HttpStatus.OK);
    }
    */

    //, headers = "Accept = application/json"
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Movie> getAvailableMovies(@PathVariable String id) {

        //int tokenState = Security.validateToken(token);

        //if (tokenState == 0)
            //return new ResponseEntity<List<Movie>>(MovieDAO.getInstance().getAvailableMovies(), HttpStatus.OK);

        return new ResponseEntity<Movie>(HttpStatus.OK);
        //return new ResponseEntity<Movie>(new Movie(1), HttpStatus.OK);
        //return new ResponseEntity<Movie>(new Movie(1), getHeaders(), HttpStatus.OK);
        //else
            //return new ResponseEntity<List<Movie>>(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Movie> insert(@RequestBody Movie data) {

        //int tokenState = Security.validateToken(token);

        //if (tokenState == 0)
        //return new ResponseEntity<List<Movie>>(MovieDAO.getInstance().getAvailableMovies(), HttpStatus.OK);

        return new ResponseEntity<Movie>(HttpStatus.OK);
        //return new ResponseEntity<Movie>(new Movie(1), HttpStatus.OK);
        //return new ResponseEntity<Movie>(new Movie(1), getHeaders(), HttpStatus.OK);
        //else
        //return new ResponseEntity<List<Movie>>(HttpStatus.UNAUTHORIZED);
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        return headers;
    }


}